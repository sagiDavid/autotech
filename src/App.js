import React, { useState } from 'react';
import { nanoid } from "nanoid";
import AddUser from './components/User/AddUser';
import UsersList from './components/User/UsersList';
import ErrorModal from './components/UI/ErrorModal';

function App() {
  const [usersList, setUsersList] = useState([]);
  const [error, setError] = useState();
  const [editUserId, setEditUserId] = useState(null);
  const [editFormData, setEditFormData] = useState({
    ID: "",
    firstname: "",
    lastname: "",
  }); 

  const addUserHandler = (uID, uFirstName, uLastName) => {
    setUsersList((prevUsersList) => {

      for (var i=0 ; i < prevUsersList.length ; i++) {
            if(prevUsersList[i].ID === uID) {
              setError({
                title: 'Invalid ID',
                message: 'The ID card exists in the system!'
            })
            return[...prevUsersList];
            }
      }
      
      return [...prevUsersList, {id: nanoid(), ID: uID, firstname: uFirstName, lastname: uLastName}];
    });
  };

  const editUserClickHandler = (event, user) => {
    event.preventDefault();

    setEditUserId(user.id);

    const formValues = {
      ID: user.ID, 
      firstname: user.firstname, 
      lastname: user.lastname
    };

    setEditFormData(formValues);
  }; 
    
  const editFormChangeHandler = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData);
  };

  const editFormSubmitHandler = (event) => {
    event.preventDefault();

    const editedUser = {
      id: editUserId,
      ID: editFormData.ID,
      firstname: editFormData.firstname,
      lastname: editFormData.lastname,
    };

    const newUsersList = [...usersList];

    const index = usersList.findIndex((user) => user.id === editUserId);

    newUsersList[index] = editedUser;

    setUsersList(newUsersList);
    setEditUserId(null);
  };

  const deleteUserHandler = (uId) => {
    const newUsersList = [...usersList];

    const index = usersList.findIndex((user) => user.id === uId);

    newUsersList.splice(index, 1);

    setUsersList(newUsersList);
  };


  const errorHandler = () => {
    setError(null);
};


  return (
    <div>
      <AddUser onAddUser={addUserHandler}/>
      {error && <ErrorModal title={error.title} message={error.message} onHandlerError={errorHandler}/>}
      <UsersList 
        editFormSubmitHandler={editFormSubmitHandler}
        editFormData={editFormData}
        editUserId={editUserId}
        editFormChangeHandler={editFormChangeHandler}
        users={usersList}  
        editUserClickHandler={editUserClickHandler} 
        deleteUserHandler={deleteUserHandler} 
        editFormSubmithandler={editFormSubmitHandler} />
    </div>
  );
}

export default App;