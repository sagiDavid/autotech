import React, {useState} from "react";
import Card from '../UI/Card';
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";
import classes from './AddUser.module.css';

const AddUser = (props) => {
    const [enteredID, setEnteredID] = useState('');
    const [enteredFirstName, setEnteredFirstName] = useState('');
    const [enteredLastName, setEnteredLastName] = useState('');
    const [error, setError] = useState();

     const addUserHandler = (event) => {
        event.preventDefault();
        
        if(enteredID.trim().length === 0 || enteredFirstName.trim().length === 0 || enteredLastName.trim().length === 0){
            setError({
                title: 'Invalid input',
                message: 'Please enter a valid ID and name (non-enpty values).'
            });
            return;
        }

        if(+enteredID < 1){
            setError({
                title: 'Invalid ID',
                message: 'Please enter a vaild ID (> 0).'
            });
            return;
        }

        if(enteredID.trim().length !== 9){
            setError({
                title: 'Invalid ID',
                message: 'Please enter a vaild lenght ID (9 digits).'
            });
            return;
        }

        props.onAddUser(enteredID, enteredFirstName, enteredLastName);
        setEnteredID('');
        setEnteredFirstName('');
        setEnteredLastName('');
    };

    const IDChangeHandler = (event) => {
        setEnteredID(event.target.value);
    };

    const fisrtNameChangeHandler = (event) => {
        setEnteredFirstName(event.target.value);
    };

    const lastNameChangeHandler = (event) => {
        setEnteredLastName(event.target.value);
    };

    const errorHandler = () => {
        setError(null);
    };


    return (
        <div>
        {error && <ErrorModal title={error.title} message={error.message} onHandlerError={errorHandler}/>}
        <Card className={classes.input}>
            <from>
                <label htmlFor="ID">ID</label>
                <input id ="ID" type="number" value={enteredID} onChange={IDChangeHandler} />
                <label htmlFor="firstName">Fisrt name</label>
                <input id ="firstName" type="text" value={enteredFirstName} onChange={fisrtNameChangeHandler} />
                <label htmlFor="lastName">Last name</label>
                <input id ="lastName" type="text" value={enteredLastName} onChange={lastNameChangeHandler} />
                <Button type="submit" onClick={addUserHandler}>Add User</Button>
            </from>
        </Card>
        </div>
    );
};

export default AddUser;