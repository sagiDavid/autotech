import React, {useState, Fragment} from "react";
import classes from "./UsersList.module.css";
import Card from "../UI/Card";
import EditUser from "./EditUser";
import Users from "./Users";

const UsersList = (props) => {

    return (
    <Card className={classes.users}>
        <form onSubmit={props.editFormSubmitHandler}>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
            <tbody>
            {props.users.map((user) => (
            <Fragment>
                {props.editUserId === user.id ? (
                <EditUser
                  editFormData={props.editFormData}
                  editFormChangeHandler={props.editFormChangeHandler}
                />
              ) : (
                <Users
                  user={user}
                  editUserClickHandler={props.editUserClickHandler}
                  deleteUserHandler={props.deleteUserHandler}
                />
              )}
            </Fragment>
          ))}
            </tbody>
        </table>
        </form>
    </Card>
    );
};

export default UsersList;