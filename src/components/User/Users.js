import React from "react";

const Users = (props) => {

  return (
    <tr>
      <td>{props.user.ID}</td>
      <td>{props.user.firstname}</td>
      <td>{props.user.lastname}</td>
      <td>
        <button
          type="button"
          onClick={(event) => props.editUserClickHandler(event, props.user)}
        >
          Edit
        </button>
        <button type="button" onClick={() => props.deleteUserHandler(props.user.id)}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default Users;