import React from "react";

const EditUser = (props) => {
   
    return (
        <tr>
            <td>
              {props.editFormData.ID}
          </td>
          <td>
            <input
              type="text"
              required="required"
              placeholder="Enter a first name..."
              name="firstname"
              value={props.editFormData.firstname}
              onChange={props.editFormChangeHandler}
            ></input>
          </td>
          <td>
            <input
              type="text"
              required="required"
              placeholder="Enter an last name..."
              name="lastname"
              value={props.editFormData.lastname}
              onChange={props.editFormChangeHandler}
            ></input>
          </td>
          <td>
            <button type="submit">Save</button>
          </td>
        </tr>
      );
};

export default EditUser;